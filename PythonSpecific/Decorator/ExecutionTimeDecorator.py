import time, logging

logging.basicConfig(
	level = logging.DEBUG, 
	filename = 'ExecutionProgramLog.txt', 
	format = ' %(asctime)s - %(levelname)s - %(message)s')

def executionTimeDecorator(func):
	def inner():
		firstTime = time.time()
		result = func()
		lastTime = time.time()

		print('%2.2f saniyede calisti.' % (lastTime - firstTime))

		return result

	return inner
		
def executionTimeDecoratorWithLogging(func):
	def inner():
		firstTime = time.time()
		result = func()
		lastTime = time.time()

		logging.debug('%2.2f sec' % (lastTime - firstTime))

		return result

	return inner


@executionTimeDecorator
def calculate():
	time.sleep(2.05)
	print("Calculate fonksiyonu calisiyor...")

@executionTimeDecoratorWithLogging
def calculateWithLogging():
	time.sleep(8.42)
	print("CalculateLogging fonksiyonu calisiyor...")

calculate()
calculateWithLogging()

__**Kodlar Çalıştırılırken Dikkat Edilecek Hususlar**__ 
> :pushpin: Computation klasörü içerisinde bulunan **fibonacci** ve **factorial** kodlarını çalıştırmak için şu yöntem izlenmelidir:
>> __**python3 dosyaismi.py testedilecekrakam**__

__**Telefon Rehberi Uygulaması Görselleri**__
> __Ana Sayfa__
>> ![IMAGE](https://i.hizliresim.com/bG3WEn.jpg)
> __İçeriğin Görüntülenmesi__
>> ![IMAGE](https://i.hizliresim.com/VMdbNR.jpg)
> __Rehbere Yeni Kişi Ekleme__
>> ![IMAGE](https://i.hizliresim.com/ERoZM9.jpg)
> __Kayıt Güncelleme__
>> ![IMAGE](https://i.hizliresim.com/1g5X31.jpg)

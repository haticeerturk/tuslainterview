def quickSort(array, first, last):
	if(first < last):
		pIndex = quickPartition(array, first, last)
		quickSort(array, first, pIndex - 1)
		quickSort(array, pIndex + 1, last)

	else:
		return

def quickPartition(array, first, last):
	pivot = array[last]
	pIndex = first

	for i in range(first, last):
		if(array[i] <= pivot):
			temp = array[i]
			array[i] = array[pIndex]
			array[pIndex] = temp
			pIndex += 1

	temp = array[pIndex]
	array[pIndex] = array[last]
	array[last] = temp
	return pIndex


array = [54,26,90,17,77,31,44,53,21]
quickSort(array, 0, len(array)-1)
print(array)

import sys

number = int(sys.argv[1])

fib1, fib2 = 1, 1
isFib = None

def isFibonacci(number):
	global isFib
	global fib1
	global fib2
	fib = fib1 + fib2
	
	if(fib != number and fib < number):
		fib1 = fib2
		fib2 = fib
		isFibonacci(number)
	elif(fib == number):
		isFib = True

isFibonacci(number)
if(isFib):
	print(number," is a Fibonacci number.")
else:
	print(number," is not a Fibonacci number.")
